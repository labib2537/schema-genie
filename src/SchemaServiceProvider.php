<?php

namespace Genie\Giniesschema\Schema;

use Illuminate\Support\ServiceProvider;

class SchemaServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'schema');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    
\Illuminate\Support\Facades\Blade::component('forms.select-connection', \Genie\Giniesschema\Schema\App\View\Components\Forms\SelectConnection::class);

\Illuminate\Support\Facades\Blade::component('forms.select-table', \Genie\Giniesschema\Schema\App\View\Components\Forms\SelectTable::class);

\Illuminate\Support\Facades\Blade::component('forms.select-structure', \Genie\Giniesschema\Schema\App\View\Components\Forms\SelectStructure::class);

\Illuminate\Support\Facades\Blade::component('forms.select-constraint', \Genie\Giniesschema\Schema\App\View\Components\Forms\SelectConstraint::class);
##||ANOTHERCOMPONENT||##
    
    }
    public function register()
    { }
}