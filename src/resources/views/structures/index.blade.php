<x-sg-master>
    {{-- @dd($key); --}}
   
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Structure') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="structureDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                        <th>{{ __('Column') }}</th>
                        <th>{{ __('Data Type') }}</th>
                        <th>{{ __('Lenght') }}</th>
                        <th>{{ __('Default') }}</th>
                        <th>{{ __('unsigned') }}</th>
                        <th>{{ __('comment') }}</th>
                        <th>{{ __('Not null') }}</th>
                        <th>{{'precision'}}</th>						
                        <th>{{'scale'}}</th>						
                        <th>{{'autoincrement'}}</th>						
                        <th>{{'columnDefinition'}}</th>
						
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @php                  
                        $counter = 0;                       
                    @endphp
                    @foreach ($columns as $index=>$column)
                    @php
                        $attr = $column->toArray();
                    @endphp
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$index}}</td>
                        <td>{{$types[$counter]}}</td>
                        <td>{{$attr['length']}}</td>						
                        <td>{{$attr['default']?'Y':'N'}}</td>						
                        <td>{{$attr['unsigned']?'Y':'N'}}</td>						
                        <td>{{$attr['comment']}}</td>						
                        <td>{{$attr['notnull']?'Y':'N'}}</td>						
                        <td>{{$attr['precision']}}</td>						
                        <td>{{$attr['scale']}}</td>						
                        <td>{{$attr['autoincrement']?'Y':'N'}}</td>						
                        <td>{{$attr['columnDefinition']}}</td>						
                    </tr>
                    @php
                        $counter++
                    @endphp
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('structures.create')}}" />
            

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#structureDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
{{-- "name" => "ID"
"type" => Doctrine\DBAL\Types\IntegerType {#1454}
"default" => null
"notnull" => true
"length" => null
"precision" => 10
"scale" => 0
"fixed" => false
"unsigned" => false
"autoincrement" => false
"columnDefinition" => null
"comment" => null 
{{ isset($coveringletter->source_branch) ? $coveringletter->source_branch : null }}
--}}