<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Structure') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('structures.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                {{--relationalFields--}}

                    <!-- column -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="columnInput">{{ __('Column') }}</x-sg-label>
        <x-sg-text type="text" id="columnInput" name="column" :value="old('column')" placeholder="" />
        <x-sg-alert-errors name="column" />
    </div>
</div>

<!-- data_type -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="data_typeInput">{{ __('Data Type') }}</x-sg-label>
        <x-sg-text type="text" id="data_typeInput" name="data_type" :value="old('data_type')" placeholder="" />
        <x-sg-alert-errors name="data_type" />
    </div>
</div>

<!-- lenght -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="lenghtInput">{{ __('Lenght') }}</x-sg-label>
        <x-sg-text type="text" id="lenghtInput" name="lenght" :value="old('lenght')" placeholder="" />
        <x-sg-alert-errors name="lenght" />
    </div>
</div>

<!-- collation -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="collationInput">{{ __('Collation') }}</x-sg-label>
        <x-sg-text type="text" id="collationInput" name="collation" :value="old('collation')" placeholder="" />
        <x-sg-alert-errors name="collation" />
    </div>
</div>

<!-- attributes -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="attributesInput">{{ __('Attributes') }}</x-sg-label>
        <x-sg-text type="text" id="attributesInput" name="attributes" :value="old('attributes')" placeholder="" />
        <x-sg-alert-errors name="attributes" />
    </div>
</div>

<!-- null -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="nullInput">{{ __('Null') }}</x-sg-label>
        <x-sg-text type="text" id="nullInput" name="null" :value="old('null')" placeholder="" />
        <x-sg-alert-errors name="null" />
    </div>
</div>

<!-- default -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="defaultInput">{{ __('Default') }}</x-sg-label>
        <x-sg-text type="text" id="defaultInput" name="default" :value="old('default')" placeholder="" />
        <x-sg-alert-errors name="default" />
    </div>
</div>

<!-- comment -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="commentInput">{{ __('Comment') }}</x-sg-label>
        <x-sg-text type="text" id="commentInput" name="comment" :value="old('comment')" placeholder="" />
        <x-sg-alert-errors name="comment" />
    </div>
</div>

<!-- extra -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="extraInput">{{ __('Extra') }}</x-sg-label>
        <x-sg-text type="text" id="extraInput" name="extra" :value="old('extra')" placeholder="" />
        <x-sg-alert-errors name="extra" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('structures.index')}}" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
