<x-sg-master>
    @php
$jsonString = json_encode($jsonData);
@endphp
 

        <form action="{{ route('create_json') }}" method="post">
            @csrf
            <input type="hidden" name="json" value="{{$jsonString}}">
            <input type="hidden" name="key" value="{{$value}}">
            <button type="submit" class="btn btn-primary">Store</button>
        </form>

        @php
            echo "<pre>";
                print_r($jsonString);
            echo"</pre>";
        @endphp
</x-sg-master>