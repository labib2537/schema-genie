<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Structure') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="structureDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                            						<th>{{ __('Column') }}</th>
						<th>{{ __('Data Type') }}</th>
						<th>{{ __('Lenght') }}</th>
						<th>{{ __('Collation') }}</th>
						<th>{{ __('Null') }}</th>
						<th>{{ __('Default') }}</th>
						<th>{{ __('Comment') }}</th>
						<th>{{ __('Extra') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($structures as $structure)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        						<td>{{ $structure->column }}</td>
						<td>{{ $structure->data_type }}</td>
						<td>{{ $structure->lenght }}</td>
						<td>{{ $structure->collation }}</td>
						<td>{{ $structure->null }}</td>
						<td>{{ $structure->default }}</td>
						<td>{{ $structure->comment }}</td>
						<td>{{ $structure->extra }}</td>

                        <td>
                            <x-sg-link-show href="{{route('structures.show', $structure->uuid)}}" />
                            <x-sg-link-edit href="{{route('structures.edit', $structure->uuid)}}" />
                            <x-sg-btn-delete action="{{route('structures.destroy', $structure->uuid)}}" method="post" />
                            <x-sg-link-history uuid="{{ $structure->uuid }}"  api="/api/structure-histories-list" />
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('structures.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#structureDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
