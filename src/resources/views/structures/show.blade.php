<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Structure') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Column') }} : </b> {{ $structure->column }}</p>
			<p><b>{{ __('Data Type') }} : </b> {{ $structure->data_type }}</p>
			<p><b>{{ __('Lenght') }} : </b> {{ $structure->lenght }}</p>
			<p><b>{{ __('Collation') }} : </b> {{ $structure->collation }}</p>
			<p><b>{{ __('Attributes') }} : </b> {{ $structure->attributes }}</p>
			<p><b>{{ __('Null') }} : </b> {{ $structure->null }}</p>
			<p><b>{{ __('Default') }} : </b> {{ $structure->default }}</p>
			<p><b>{{ __('Comment') }} : </b> {{ $structure->comment }}</p>
			<p><b>{{ __('Extra') }} : </b> {{ $structure->extra }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('structures.index')}}" />
            <x-sg-link-list href="{{route('structures.index')}}" />
            <x-sg-link-edit href="{{route('structures.edit',$structure->uuid)}}" />
            <x-sg-link-history uuid="{{$structure->uuid}}" api="/api/structures-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
