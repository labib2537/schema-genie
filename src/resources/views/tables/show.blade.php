<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Table') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Title') }} : </b> {{ $table->title }}</p>
			<p><b>{{ __('Rows') }} : </b> {{ $table->rows }}</p>
			<p><b>{{ __('Type') }} : </b> {{ $table->type }}</p>
			<p><b>{{ __('Collation') }} : </b> {{ $table->collation }}</p>
			<p><b>{{ __('Data length') }} : </b> {{ $table->data length }}</p>
			<p><b>{{ __('Index length') }} : </b> {{ $table->index length }}</p>
			<p><b>{{ __('Data free') }} : </b> {{ $table->data free }}</p>
			<p><b>{{ __('Auto increment') }} : </b> {{ $table->auto increment }}</p>
			<p><b>{{ __('Comment') }} : </b> {{ $table->comment }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('tables.index')}}" />
            <x-sg-link-list href="{{route('tables.index')}}" />
            <x-sg-link-edit href="{{route('tables.edit',$table->uuid)}}" />
            <x-sg-link-history uuid="{{$table->uuid}}" api="/api/tables-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
