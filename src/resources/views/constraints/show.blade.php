<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Constraint') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Key Name') }} : </b> {{ $constraint->key_name }}</p>
			<p><b>{{ __('Type') }} : </b> {{ $constraint->type }}</p>
			<p><b>{{ __('Unique') }} : </b> {{ $constraint->unique }}</p>
			<p><b>{{ __('Packed') }} : </b> {{ $constraint->packed }}</p>
			<p><b>{{ __('Column') }} : </b> {{ $constraint->column }}</p>
			<p><b>{{ __('Cardinality') }} : </b> {{ $constraint->cardinality }}</p>
			<p><b>{{ __('Collation') }} : </b> {{ $constraint->collation }}</p>
			<p><b>{{ __('Null') }} : </b> {{ $constraint->null }}</p>
			<p><b>{{ __('Comment') }} : </b> {{ $constraint->comment }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('constraints.index')}}" />
            <x-sg-link-list href="{{route('constraints.index')}}" />
            <x-sg-link-edit href="{{route('constraints.edit',$constraint->uuid)}}" />
            <x-sg-link-history uuid="{{$constraint->uuid}}" api="/api/constraints-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
