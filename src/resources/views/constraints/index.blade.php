<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Constraint') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="constraintDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                            						<th>{{ __('Key Name') }}</th>
						<th>{{ __('Type') }}</th>
						<th>{{ __('Unique') }}</th>
						<th>{{ __('Packed') }}</th>
						<th>{{ __('Column') }}</th>
						<th>{{ __('Cardinality') }}</th>
						<th>{{ __('Collation') }}</th>
						<th>{{ __('Comment') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($constraints as $constraint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        						<td>{{ $constraint->key_name }}</td>
						<td>{{ $constraint->type }}</td>
						<td>{{ $constraint->unique }}</td>
						<td>{{ $constraint->packed }}</td>
						<td>{{ $constraint->column }}</td>
						<td>{{ $constraint->cardinality }}</td>
						<td>{{ $constraint->collation }}</td>
						<td>{{ $constraint->comment }}</td>

                        <td>
                            <x-sg-link-show href="{{route('constraints.show', $constraint->uuid)}}" />
                            <x-sg-link-edit href="{{route('constraints.edit', $constraint->uuid)}}" />
                            <x-sg-btn-delete action="{{route('constraints.destroy', $constraint->uuid)}}" method="post" />
                            <x-sg-link-history uuid="{{ $constraint->uuid }}"  api="/api/constraint-histories-list" />
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('constraints.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#constraintDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
