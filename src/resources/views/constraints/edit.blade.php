<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Constraint') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('constraints.update', $constraint->uuid) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="row">
                    {{--relationalFields--}}
                    <!-- key_name -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="key_nameInput">{{ __('Key Name') }}</x-sg-label>
        <x-sg-text type="text" id="key_nameInput" name="key_name" :value="old('key_name', $constraint->key_name)" placeholder="" />
        <x-sg-alert-errors name="key_name" />
    </div>
</div>

<!-- type -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="typeInput">{{ __('Type') }}</x-sg-label>
        <x-sg-text type="text" id="typeInput" name="type" :value="old('type', $constraint->type)" placeholder="" />
        <x-sg-alert-errors name="type" />
    </div>
</div>

<!-- unique -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="uniqueInput">{{ __('Unique') }}</x-sg-label>
        <x-sg-text type="text" id="uniqueInput" name="unique" :value="old('unique', $constraint->unique)" placeholder="" />
        <x-sg-alert-errors name="unique" />
    </div>
</div>

<!-- packed -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="packedInput">{{ __('Packed') }}</x-sg-label>
        <x-sg-text type="text" id="packedInput" name="packed" :value="old('packed', $constraint->packed)" placeholder="" />
        <x-sg-alert-errors name="packed" />
    </div>
</div>

<!-- column -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="columnInput">{{ __('Column') }}</x-sg-label>
        <x-sg-text type="text" id="columnInput" name="column" :value="old('column', $constraint->column)" placeholder="" />
        <x-sg-alert-errors name="column" />
    </div>
</div>

<!-- cardinality -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="cardinalityInput">{{ __('Cardinality') }}</x-sg-label>
        <x-sg-text type="text" id="cardinalityInput" name="cardinality" :value="old('cardinality', $constraint->cardinality)" placeholder="" />
        <x-sg-alert-errors name="cardinality" />
    </div>
</div>

<!-- collation -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="collationInput">{{ __('Collation') }}</x-sg-label>
        <x-sg-text type="text" id="collationInput" name="collation" :value="old('collation', $constraint->collation)" placeholder="" />
        <x-sg-alert-errors name="collation" />
    </div>
</div>

<!-- null -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="nullInput">{{ __('Null') }}</x-sg-label>
        <x-sg-text type="text" id="nullInput" name="null" :value="old('null', $constraint->null)" placeholder="" />
        <x-sg-alert-errors name="null" />
    </div>
</div>

<!-- comment -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="commentInput">{{ __('Comment') }}</x-sg-label>
        <x-sg-text type="text" id="commentInput" name="comment" :value="old('comment', $constraint->comment)" placeholder="" />
        <x-sg-alert-errors name="comment" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('constraints.index')}}" />
        </x-slot>
    </x-sg-card>
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
