<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Connection') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Title') }} : </b> {{ $connection->title }}</p>
			<p><b>{{ __('Host Db') }} : </b> {{ $connection->host_db }}</p>
			<p><b>{{ __('User') }} : </b> {{ $connection->user }}</p>
			<p><b>{{ __('Password') }} : </b> {{ $connection->password }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('connections.index')}}" />
            <x-sg-link-list href="{{route('connections.index')}}" />
            <x-sg-link-edit href="{{route('connections.edit',$connection->uuid)}}" />
            <x-sg-link-history uuid="{{$connection->uuid}}" api="/api/connections-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
