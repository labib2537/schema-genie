<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Connection') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('connections.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <x-forms.select-connection :attributes="['name' => 'householdtype', 'class' => 'form-control select', 'id' => 'householdtype']" :selected="old('')" />
                    <x-sg-alert-errors name="householdtype" />
                <div class="row">
                {{--relationalFields--}}

                    <!-- title -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="titleInput">{{ __('Title') }}</x-sg-label>
        <x-sg-text type="text" id="titleInput" name="title" :value="old('title')" placeholder="" />
        <x-sg-alert-errors name="title" />
    </div>
</div>

<!-- host_db -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="host_dbInput">{{ __('Host Db') }}</x-sg-label>
        <x-sg-text type="text" id="host_dbInput" name="host_db" :value="old('host_db')" placeholder="" />
        <x-sg-alert-errors name="host_db" />
    </div>
</div>

<!-- user -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="userInput">{{ __('User') }}</x-sg-label>
        <x-sg-text type="text" id="userInput" name="user" :value="old('user')" placeholder="" />
        <x-sg-alert-errors name="user" />
    </div>
</div>

<!-- password -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="passwordInput">{{ __('Password') }}</x-sg-label>
        <x-sg-text type="text" id="passwordInput" name="password" :value="old('password')" placeholder="" />
        <x-sg-alert-errors name="password" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('connections.index')}}" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
