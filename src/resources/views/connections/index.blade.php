<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Connection') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="connectionDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                        <th>{{ __('Title') }}</th>
						<th>{{ __('Host Db') }}</th>
						<th>{{ __('User') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($connections as $key=>$connection)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $connection}}</td>
						<td></td>
						<td></td>

                        <td>
                        <x-sg-link-show href="{{route('table', $key)}}" />
                            
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('connections.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#connectionDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
