<?php

namespace Genie\Giniesschema\Schema\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;


use App\Traits\Historiable;

class Constraint extends Model
{
    use UserTrackable;
    use SoftDeletes;
    
    
    use Historiable;
    protected $connection = 'oracle';
    protected $table = 'constraints';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}
