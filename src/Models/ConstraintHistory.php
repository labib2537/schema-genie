<?php

namespace Genie\Giniesschema\Schema\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserTrackable;


class ConstraintHistory extends Model
{
    protected $table = 'constraint_histories';
    protected $guarded = ['id'];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
