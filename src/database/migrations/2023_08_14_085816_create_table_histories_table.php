<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('oracle')->create('table_histories', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('table_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('title', '128')->nullable();
			$table->string('rows')->nullable();
			$table->string('type', '32')->nullable();
			$table->string('collation', '64')->nullable();
			$table->string('data_length')->nullable();
			$table->string('index_length')->nullable();
			$table->string('data_free')->nullable();
			$table->string('auto_increment')->nullable();
            $table->string('comment')->nullable();
			$table->text('json_migration')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('oracle')->dropIfExists('table_histories');
    }
}