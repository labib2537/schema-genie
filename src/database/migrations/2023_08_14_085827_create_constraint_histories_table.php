<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConstraintHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('oracle')->create('constraint_histories', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('constraint_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('key_name', '128')->nullable();
			$table->string('type', '32')->nullable();
			$table->string('unique', '32')->nullable();
			$table->string('packed', '32')->nullable();
			$table->string('column', '32')->nullable();
			$table->string('cardinality')->nullable();
			$table->string('collation', '32')->nullable();
			$table->string('null', '32')->nullable();
			$table->string('comment', '128')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('oracle')->dropIfExists('constraint_histories');
    }
}