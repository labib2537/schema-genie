<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('oracle')->create('connection_histories', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('connection_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('title', '128')->nullable();
			$table->string('host_db', '128')->nullable();
			$table->string('user', '64')->nullable();
			$table->string('password', '64')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('oracle')->dropIfExists('connection_histories');
    }
}