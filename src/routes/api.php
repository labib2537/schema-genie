<?php 
use Illuminate\Support\Facades\Route;
use Genie\Giniesschema\Schema\Http\Controllers\Api\ConnectionController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\ConnectionHistoryController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\TableController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\TableHistoryController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\StructureController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\StructureHistoryController;
use Genie\Giniesschema\Schema\Http\Controllers\Api\ConstraintController;

use Genie\Giniesschema\Schema\Http\Controllers\Api\ConstraintHistoryController;
//use namespace

Route::group(['middleware' => 'api', 'prefix' => 'api', 'as' => 'api.'], function () {
	Route::apiResource('connections', ConnectionController::class);
	Route::get('connection-histories-list/{uuid}', [ConnectionHistoryController::class,'list']);
	Route::apiResource('connection-histories', ConnectionHistoryController::class);
	Route::apiResource('tables', TableController::class);
	Route::get('table-histories-list/{uuid}', [TableHistoryController::class,'list']);
	Route::apiResource('table-histories', TableHistoryController::class);
	Route::apiResource('structures', StructureController::class);
	Route::get('structure-histories-list/{uuid}', [StructureHistoryController::class,'list']);
	Route::apiResource('structure-histories', StructureHistoryController::class);
	Route::apiResource('constraints', ConstraintController::class);
	Route::get('constraint-histories-list/{uuid}', [ConstraintHistoryController::class,'list']);
	Route::apiResource('constraint-histories', ConstraintHistoryController::class);
//Place your route here
});