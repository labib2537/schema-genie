<?php

use Illuminate\Support\Facades\Route;
use Genie\Giniesschema\Schema\Http\Controllers\ConnectionController;
use Genie\Giniesschema\Schema\Http\Controllers\TableController;
use Genie\Giniesschema\Schema\Http\Controllers\StructureController;

use Genie\Giniesschema\Schema\Http\Controllers\ConstraintController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('connections', ConnectionController::class);
	Route::resource('tables', TableController::class);
	Route::resource('structures', StructureController::class);
	Route::resource('constraints', ConstraintController::class);
	Route::get('/columns/{key}',[StructureController::class, 'structure'])->name('structure');
	Route::get('/editablestructure/{key}',[StructureController::class, 'editablestructure'])->name('editablestructure');
	Route::post('/editablestructure',[StructureController::class, 'editablestructurestore'])->name('editablestructure.store');
	Route::get('/json-structure/{key}',[StructureController::class, 'jsonStructure'])->name('structures.json');
	Route::get('/table/{key}',[TableController::class, 'tables'])->name('table');

	Route::post('create-json', [StructureController::class, 'createJson'])->name('create_json');
	Route::post('store', [StructureController::class, 'store'])->name('json_store');


//Place your route here
});