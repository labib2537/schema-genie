<?php

namespace Genie\Giniesschema\Schema\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConstraintRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}