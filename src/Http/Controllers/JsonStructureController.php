<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JsonStructureController extends Controller
{
    public function jsonStructure()
    {
       return view('schema::structures.json'); 
    }
}
