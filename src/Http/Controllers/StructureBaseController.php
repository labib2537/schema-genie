<?php

namespace Genie\Giniesschema\Schema\Http\Controllers;

use App\Http\Controllers\Controller;
use Genie\Giniesschema\Schema\Http\Requests\StructureRequest;
use Genie\Giniesschema\Schema\Models\Structure;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class StructureBaseController extends Controller
{

    public static $visiblePermissions = [
        'index' => 'List',
        'create' => 'Create Form',
        'store' => 'Save',
        'show' => 'Details',
        'edit' => 'Edit Form',
        'update' => 'Update',
        'destroy' => 'Delete'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('schema::structures.index', [
                        'structures' => Structure::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schema::structures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StructureRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StructureRequest $request)
    {
        try {
            $structure = Structure::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('structures.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function show(Structure $structure)
    {
        return view('schema::structures.show', compact('structure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function edit(Structure $structure)
    {
        return view('schema::structures.edit', compact('structure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StructureRequest  $request
     * @param  \App\Models\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function update(StructureRequest $request, Structure $structure)
    {
        try {
            $structure->update($request->all());
            //handle relationship update
            return redirect()->route('structures.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Structure $structure)
    {
        try {
            $structure->delete();

            return redirect()->route('structures.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
