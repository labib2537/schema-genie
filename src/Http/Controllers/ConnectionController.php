<?php

namespace Genie\Giniesschema\Schema\Http\Controllers;
use App\Http\Controllers\Controller;
use Genie\Giniesschema\Schema\Http\Requests\ConnectionRequest;
use Genie\Giniesschema\Schema\Models\Connection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ConnectionController extends ConnectionBaseController
{
    public function index()
    {
        $schema = DB::connection()->getDoctrineSchemaManager();
        $connections = $schema->listDatabases();
        return view('schema::connections.index', compact('connections'));
    }
    
    public function store(ConnectionRequest $request)
    {
        try {
            $connection = Connection::create(['uuid'=>(string) Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('connections.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
}
