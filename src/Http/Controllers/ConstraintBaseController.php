<?php

namespace Genie\Giniesschema\Schema\Http\Controllers;

use App\Http\Controllers\Controller;
use Genie\Giniesschema\Schema\Http\Requests\ConstraintRequest;
use Genie\Giniesschema\Schema\Models\Constraint;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class ConstraintBaseController extends Controller
{

    public static $visiblePermissions = [
        'index' => 'List',
        'create' => 'Create Form',
        'store' => 'Save',
        'show' => 'Details',
        'edit' => 'Edit Form',
        'update' => 'Update',
        'destroy' => 'Delete'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('schema::constraints.index', [
                        'constraints' => Constraint::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schema::constraints.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ConstraintRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConstraintRequest $request)
    {
        try {
            $constraint = Constraint::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('constraints.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function show(Constraint $constraint)
    {
        return view('schema::constraints.show', compact('constraint'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function edit(Constraint $constraint)
    {
        return view('schema::constraints.edit', compact('constraint'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ConstraintRequest  $request
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function update(ConstraintRequest $request, Constraint $constraint)
    {
        try {
            $constraint->update($request->all());
            //handle relationship update
            return redirect()->route('constraints.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Constraint $constraint)
    {
        try {
            $constraint->delete();

            return redirect()->route('constraints.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
