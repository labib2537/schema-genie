<?php
namespace Genie\Giniesschema\Schema\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Genie\Giniesschema\Schema\Models\Structure;
use Genie\Giniesschema\Schema\Models\StructureHistory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


class StructureHistoryController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Response status code
    |--------------------------------------------------------------------------
    | 201 response with created data
    | 200 update/list/show/delete
    | 204 deleted response with no content
    | 500 internal server or db error
    */

    public static $visiblePermissions = [
        'index' => 'All Histories of all items',
        'list' => 'All Histories of specific item',
        'show'  => 'Detail of a history of an item',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($uuid)
    {
        // \DB::enableQueryLog();
        // dd(\DB::getQueryLog());
        try{
            $histories = StructureHistory::where('uuid',$uuid)->get();

            return response()->json([
                'status' => true,
                'data' => $histories
            ], 200);

        }catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $histories = StructureHistory::latest()->get();

        return response()->json([
            'status' => true,
            'data' => $histories
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(StructureHistory $history)
    {
         return response()->json([
            'status' => true,
            'data' => $history
         ], 200);
    }

}

