<?php

namespace Genie\Giniesschema\Schema\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Genie\Giniesschema\Schema\Http\Requests\StructureRequest;
use Genie\Giniesschema\Schema\Models\Structure;

class StructureController extends StructureBaseController
{
    public function structure($key)
    {
        // dd($key);
        $value = $key;
        $dbalDataType = ['Doctrine\DBAL\Types\IntegerType' => 'int',
        'Doctrine\DBAL\Types\StringType' => 'string',
        'Doctrine\DBAL\Types\DecimalType'=> 'decimal',
        "Doctrine\DBAL\Types\DateTimeType"=>'date-time'
       ];        
    
        $schema = DB::connection()->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        // dd($tables);
        $columns = $schema->listTableColumns($tables[$key]);
        // dd($columns);
        $types = [];
         foreach($columns as $key=>$column){
            // dd($column->toArray());
            $columnNames[$key] = $column->getType();
            $types[] = $dbalDataType[get_class($columnNames[$key])];
        }

        // dd($value);


    //   dd($dbalDataType[get_class($columnNames[4])]);
        return view('schema::structures.index',compact('columns','types','value'));
    }

    public function editableStructure($key)
    {
        $dbalDataType = ['Doctrine\DBAL\Types\IntegerType' => 'int',
        'Doctrine\DBAL\Types\StringType' => 'string',
        'Doctrine\DBAL\Types\DecimalType'=> 'decimal',
        "Doctrine\DBAL\Types\DateTimeType"=>'date-time'
       ];        
    
        $schema = DB::connection()->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        $columns = $schema->listTableColumns($tables[$key]);
        $types = [];
         foreach($columns as $key=>$column){
            $columnNames[$key] = $column->getType();
            $types[] = $dbalDataType[get_class($columnNames[$key])];
        }


    //   dd($dbalDataType[get_class($columnNames[4])]);
        return view('schema::structures.editablestructure',compact('columns','types'));
    }

    public function editableStructurestore(StructureRequest $request)
    {
        dd($request->all());
        
    }

    public function jsonStructure($key){
        $value = $key;
        $dbalDataType = ['Doctrine\DBAL\Types\IntegerType' => 'int',
        'Doctrine\DBAL\Types\StringType' => 'string',
        'Doctrine\DBAL\Types\DecimalType'=> 'decimal',
        "Doctrine\DBAL\Types\DateTimeType"=>'date-time'
       ];        
    
        $schema = DB::connection()->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        $table = $tables[$key];        
        $columns = $schema->listTableColumns($tables[$key]);
        $jsonData = [];
         foreach($columns as $column){
            $attrtibutes = $column->toArray();
            $columnNames[$key] = $column->getType();            
            $jsonData[$attrtibutes['name']] = [                
                'dataType' => $dbalDataType[get_class($columnNames[$key])],
                'length' => $attrtibutes['length'] === 'NULL' ? null : $attrtibutes['length'],
                'precision' => $attrtibutes['precision'],
                'unsigned' => $attrtibutes['unsigned'] === 'TRUE',
                'notnull' => $attrtibutes['notnull'] === 'TRUE',
                'autoincrement' => $attrtibutes['autoincrement'],
                'columnDefinition' => $attrtibutes['columnDefinition'],
                'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
            ];
           }

        $jsonStructure = [
            "connection" => "test",
            "tableName" =>  $table,
            "options" => [
                "engine" =>  "test",
                "charset" =>  "test",
            ],
            "columns" => $jsonData,
        ];    
        $response = response()->json($jsonStructure);
        $jsonData = $response->getData();
        return view('schema::structures.json', compact('response','value','jsonData'));
        
        // return view('schema::structures.create',compact())           
    }

    public function store(StructureRequest $request)
    {
      
        // Structure::
        // dd($jsonData);
        return view('schema::structures.index',compact('jsonResponse','jsonData'));
    }
}
